﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [Header("VFX")]
    [SerializeField] private GameObject winCanvas;
    [SerializeField] private GameObject loseCanvas;
    [SerializeField] private GameObject endGameCanvas;

    [Header("SFX")]
    [SerializeField] private AudioClip winSFX;
    [SerializeField] private AudioClip loseSFX;

    private AudioSource colonnaSonora;
    private AudioSource audioSource;

    void Start()
    {
        audioSource = this.gameObject.AddComponent<AudioSource>();
        colonnaSonora = GameObject.FindGameObjectWithTag("ColonnaSonora")
            .GetComponent<AudioSource>();
        audioSource.playOnAwake = false;
        audioSource.loop = false;
        audioSource.volume = 0.6f;
        Time.timeScale = 1.0f;
    }

    private void Update()
    {
        IsGameOver();
    }

    private void IsGameOver()
    {
        if (Singleton<PlayerOne>.Instance.Health <= 0 ||
            Singleton<PlayerTwo>.Instance.Health <= 0)
            TriggerGameOver();
        else if (Singleton<PlayerOne>.Instance.IsEndGame() ||
            Singleton<PlayerTwo>.Instance.IsEndGame())
            TriggerGameWin();
    }

    private void TriggerGameOver()
    {
        if (colonnaSonora.isPlaying)
        {
            colonnaSonora.Stop();
            audioSource.PlayOneShot(loseSFX);
        }

        DisplayLoseCanvas();
    }

    private void DisplayLoseCanvas()
    {
        loseCanvas.SetActive(true);
        loseCanvas.GetComponent<Animator>().Play("Lost");
        StartCoroutine(ReloadScene());
    }

    private IEnumerator ReloadScene()
    {
        yield return new WaitForSecondsRealtime(5.0f);
        SceneController.ReloadCurrentScene();
    }

    private void TriggerGameWin()
    {
        if (colonnaSonora.isPlaying)
        {
            colonnaSonora.Stop();
            audioSource.PlayOneShot(winSFX);
            Singleton<ScoreManager>.Instance.TotalScore +=
                Singleton<PlayerOne>.Instance.Health +
                Singleton<PlayerTwo>.Instance.Health;
        }

        CookNextLevel();
    }

    private void CookNextLevel()
    {
        winCanvas.SetActive(true);
        winCanvas.GetComponent<Animator>().Play("Win");
        StartCoroutine(CookNextScene());
    }

    private IEnumerator CookNextScene()
    {
        yield return new WaitForSecondsRealtime(2.0f);
        if (SceneController.CanLoadNextScene())
            SceneController.LoadNextScene();
        else
        {
            winCanvas.SetActive(false);
            if (GameObject.FindGameObjectsWithTag("EndGameCanvas").Length < 1)
                Instantiate(endGameCanvas);
        }
    }
}
