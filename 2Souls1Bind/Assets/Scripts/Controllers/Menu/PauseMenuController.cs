﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenuController : MonoBehaviour
{
    [SerializeField] private GameObject menu;
    [SerializeField] private AnimationClip menuInClip;
    [SerializeField] private AnimationClip menuOutClip;
    [SerializeField] private Animator menuAnimator;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            ToggleMenu();
    }

    private void ToggleMenu()
    {
        if (menu.activeInHierarchy)
        {
            menu.SetActive(!menu.activeInHierarchy);
            Time.timeScale = 1.0f;
        }
        else
        {
            menu.SetActive(!menu.activeInHierarchy);
            menuAnimator.Play(menuInClip.name);
            Time.timeScale = 0.0f;
        }
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void QuitToMainMenu()
    {
        Time.timeScale = 1.0f;
        SceneController.LoadMenuScene();
    }

    public void ReladScene()
    {
        SceneController.ReloadCurrentScene();
    }
}
