﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class MenuController : MonoBehaviour
{
    [SerializeField] private GameObject menu;
    [SerializeField] private Animator menuAnimator;
    [SerializeField] private AudioMixer audioMixer;
    [SerializeField] private Slider slider;

    [SerializeField] private Image w;
    [SerializeField] private Image d;
    [SerializeField] private Image space;

    [SerializeField] private Image leftArrow;
    [SerializeField] private Image upArrow;
    [SerializeField] private Image rightCtrl;

    [SerializeField] private TextMeshProUGUI maxScore;

    private void Update()
    {
        if (Input.GetKey(KeyCode.W))
            w.color = new Color(w.color.r, w.color.g, w.color.b, 1.0f);
        else
            w.color = new Color(w.color.r, w.color.g, w.color.b, 0.117f);

        if (Input.GetKey(KeyCode.D))
            d.color = new Color(w.color.r, w.color.g, w.color.b, 1.0f);
        else
            d.color = new Color(w.color.r, w.color.g, w.color.b, 0.117f);

        if (Input.GetKey(KeyCode.Space))
            space.color = new Color(w.color.r, w.color.g, w.color.b, 1.0f);
        else
            space.color = new Color(w.color.r, w.color.g, w.color.b, 0.117f);

        if (Input.GetKey(KeyCode.UpArrow))
            upArrow.color = new Color(w.color.r, w.color.g, w.color.b, 1.0f);
        else
            upArrow.color = new Color(w.color.r, w.color.g, w.color.b, 0.117f);

        if (Input.GetKey(KeyCode.LeftArrow))
            leftArrow.color = new Color(w.color.r, w.color.g, w.color.b, 1.0f);
        else
            leftArrow.color = new Color(w.color.r, w.color.g, w.color.b, 0.117f);

        if (Input.GetKey(KeyCode.RightControl))
            rightCtrl.color = new Color(w.color.r, w.color.g, w.color.b, 1.0f);
        else
            rightCtrl.color = new Color(w.color.r, w.color.g, w.color.b, 0.117f);

        if (Input.GetKey(KeyCode.RightCommand))
            rightCtrl.color = new Color(w.color.r, w.color.g, w.color.b, 1.0f);
        else
            rightCtrl.color = new Color(w.color.r, w.color.g, w.color.b, 0.117f);

        if (PlayerPrefs.HasKey("_BestScore"))
            maxScore.text = PlayerPrefs.GetInt("_BestScore").ToString();
        else
            maxScore.text = "0";
    }

    public void ToggleMenu()
    {
        menu.SetActive(!menu.activeInHierarchy);

        if (menu.activeInHierarchy)
        {
            Time.timeScale = 0.0f;
            menuAnimator.Play("MenuIn");
        }
        else
            Time.timeScale = 1.0f;
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void ToggleScreen()
    {
        Screen.fullScreen = !Screen.fullScreen;
    }

    public void ChangeVolume()
    {
        audioMixer.SetFloat("Volume", slider.value);
    }

    public void SetQuality(int index)
    {
        QualitySettings.SetQualityLevel(index);
    }
}
