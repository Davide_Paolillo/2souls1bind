﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartMenuController : MonoBehaviour
{
    [SerializeField] private GameObject playCanvas;

    private void Update()
    {
        if (Singleton<PlayerOne>.Instance.IsEndGame())
            TriggerPlay();
    }

    private void TriggerPlay()
    {
        playCanvas.GetComponent<Animator>().Play("PlayAnimation");
        StartCoroutine(GoToNextScene());
    }

    private IEnumerator GoToNextScene()
    {
        yield return new WaitForSecondsRealtime(1.5f);
        SceneController.LoadNextScene();
    }
}
