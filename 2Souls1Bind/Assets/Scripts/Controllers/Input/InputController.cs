﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{
    [SerializeField] private bool coordinatedMovement = true;
    [SerializeField] private bool coordinatedJump = false;
    [SerializeField] private bool coordinatedJumpMovement = false;

    private InputType inputType;

    private void Start()
    {
        RefreshInputType();
    }

    private void Update()
    {
        RefreshInputType();
        inputType.MovePlayer();
        inputType.LetPlayerJump();
    }

    private void RefreshInputType()
    {
        inputType = InputFactory.GetInputType(coordinatedMovement, coordinatedJump, coordinatedJumpMovement);
    }
}
