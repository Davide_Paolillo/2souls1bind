﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScoreManager : MonoBehaviour
{
    private TextMeshProUGUI playerScore;

    private int totalScore = 0;

    public int TotalScore { get => totalScore; set => totalScore = value; }

    private void Start()
    {
        if (FindObjectsOfType<ScoreManager>().Length == 1)
            DontDestroyOnLoad(this.gameObject);
        else
            Destroy(this.gameObject);
    }

    private void Update()
    {
        GetScoreTexts();

        if (SceneManager.GetActiveScene().buildIndex == 0)
            Destroy(this.gameObject);

        if (PlayerPrefs.HasKey("_BestScore"))
            CheckMaxScore();
        else
            PlayerPrefs.SetInt("_BestScore", TotalScore);
    }

    private void GetScoreTexts()
    {
        if (GameObject.FindGameObjectWithTag("PlayerScore"))
        {
            playerScore = GameObject.FindGameObjectWithTag("PlayerScore").GetComponent<TextMeshProUGUI>();
            playerScore.text = TotalScore.ToString();
        }
    }

    private void CheckMaxScore()
    {
        if (PlayerPrefs.GetInt("_BestScore") < TotalScore)
            PlayerPrefs.SetInt("_BestScore", TotalScore);
    }
}
