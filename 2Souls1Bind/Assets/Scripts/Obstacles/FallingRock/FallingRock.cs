﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallingRock : MonoBehaviour
{
    [SerializeField] private Rigidbody body;

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponentInParent<Player>())
            body.isKinematic = false;
    }
}
