﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rock : MonoBehaviour
{
    [SerializeField] private int damagePerHit = 1;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.GetComponentInParent<Player>())
            collision.gameObject.GetComponentInParent<Player>().TakeDamage(damagePerHit);
    }
}
