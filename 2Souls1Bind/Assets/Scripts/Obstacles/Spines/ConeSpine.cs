﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConeSpine : MonoBehaviour
{
    [SerializeField] private LayerMask playerLayer;

    private void OnCollisionEnter(Collision collision)
    {
        int collisionLayer = (int) Mathf.Log(playerLayer.value, 2.0f);

        if (collisionLayer == collision.gameObject.layer)
            collision.gameObject.GetComponentInParent<Player>().TakeFatalDamage();
    }
}
