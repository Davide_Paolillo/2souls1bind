﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lava : MonoBehaviour
{
    [SerializeField] private int damageOverSecond = 1;

    private bool notOnCooldown;

    private void Start()
    {
        notOnCooldown = true;
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.GetComponentInParent<Player>() != null &&
            other.GetComponentInParent<Player>().Health >= 0 &&
            notOnCooldown)
            StartCoroutine(DealDamage(other.gameObject));
    }

    private IEnumerator DealDamage(GameObject other)
    {
        notOnCooldown = false;
        other.GetComponentInParent<Player>().TakeDamage(damageOverSecond);
        yield return new WaitForSecondsRealtime(1.0f);
        notOnCooldown = true;
    }
}
