﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CylinderProjectile : Projectile
{
    private void OnCollisionEnter(Collision collision)
    {
        Player player;

        if (collision.transform.parent.TryGetComponent(out player))
            player.TakeDamage(this.Damage);

        DestroyProjectile();
    }

    protected override void Shoot()
    {
        float paddedSpeed = (this.Speed / SPEED_FORCE_FACTOR_MULTIPLIER) * Time.deltaTime;
        Vector3 direction = new Vector3(Vector3.left.x,
            -0.05f, 0.0f);

        this.transform.Translate(direction * paddedSpeed);
    }
}
