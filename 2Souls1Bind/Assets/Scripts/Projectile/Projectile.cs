﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Projectile : MonoBehaviour
{
    [Header("Statistics")]
    [SerializeField] private int damage = 1;
    [SerializeField] private int speed = 1;

    protected Rigidbody rigidBody;

    protected const float MINIMUM_VELOCITY_TO_SHOOT = 0.02f;
    protected const int SPEED_FORCE_FACTOR_MULTIPLIER = 600;
    protected const float TIME_TO_LIVE = 10;

    public int Speed { get => speed; }
    public int Damage { get => damage; }

    private void Start()
    {
        speed *= SPEED_FORCE_FACTOR_MULTIPLIER;

        rigidBody = this.GetComponent<Rigidbody>();
    }

    private void Update()
    {
        Shoot();
    }

    protected abstract void Shoot();

    protected virtual void DestroyProjectile()
    {
        Destroy(this.gameObject);
    }

    protected virtual void DeactivateProjectile()
    {
        Color initialColor = this.GetComponent<Renderer>().material.GetColor("_BaseColor");
        Color deactivatedColor = new Color(initialColor.r, initialColor.g, initialColor.b, 0.004f);
        this.GetComponent<Renderer>().material.SetColor("_BaseColor", deactivatedColor);
        this.damage = 0;
    }

    protected virtual IEnumerator KillAtEndOfTimeToLive()
    {
        yield return new WaitForSeconds(TIME_TO_LIVE);
        Destroy(this.gameObject);
    }
}
