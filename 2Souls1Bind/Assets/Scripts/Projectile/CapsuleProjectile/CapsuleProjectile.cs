﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapsuleProjectile : Projectile
{
    private void OnCollisionEnter(Collision collision)
    {
        Enemy enemy;

        if (collision.transform.parent.TryGetComponent(out enemy))
            enemy.TakeDamage(this.Damage);

        DestroyProjectile();
    }

    protected override void Shoot()
    {
        Vector3 forwardShot = Vector3.right * Speed;

        if (this.rigidBody.velocity.magnitude < MINIMUM_VELOCITY_TO_SHOOT)
            this.rigidBody.AddRelativeForce(forwardShot);

        StartCoroutine(KillAtEndOfTimeToLive());
    }
}
