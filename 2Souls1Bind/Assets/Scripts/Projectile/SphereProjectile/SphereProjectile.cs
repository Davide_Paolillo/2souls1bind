﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereProjectile : Projectile
{
    private GameObject target;

    public GameObject Target { get => target; set => target = value; }

    private const int TRANSPARENT_FX_LAYER = 1;

    private void OnCollisionEnter(Collision collision)
    {
        Player player;

        if (collision.transform.parent.TryGetComponent(out player))
            player.TakeDamage(this.Damage);

        DestroyProjectile();
    }

    protected override void Shoot()
    {
        if (target != null && target.layer != TRANSPARENT_FX_LAYER)
            MoveProjectile();
        else
        {
            MoveProjectile();
            DeactivateProjectile();
        } 
    }

    private void MoveProjectile()
    {
        float paddedSpeed = (this.Speed / SPEED_FORCE_FACTOR_MULTIPLIER) * Time.deltaTime;

        this.transform.position = Vector3.MoveTowards(this.transform.position,
                        target.transform.position, paddedSpeed);
    }
}
