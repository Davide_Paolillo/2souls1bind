﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Score : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI scoreText;

    private Player currentPlayer;

    private int currentScore;

    public int CurrentScore { get => currentScore; }

    private void Start()
    {
        currentPlayer = GetComponent<Player>();
    }

    private void Update()
    {
        currentScore = currentPlayer.Health;
        scoreText.text = currentPlayer.Health.ToString();
    }
}
