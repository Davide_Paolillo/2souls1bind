﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Feet : MonoBehaviour
{
    private bool isGround;

    public bool IsGround { get => isGround; }

    private void Start()
    {
        isGround = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        isGround = true;
    }

    private void OnTriggerStay(Collider other)
    {
        isGround = true;
    }

    private void OnTriggerExit(Collider other)
    {
        isGround = false;
    }
}
