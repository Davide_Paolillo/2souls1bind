﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Body : MonoBehaviour
{
    [SerializeField] private LayerMask playerLayer;

    private bool arePlayerJoined;

    public bool ArePlayerJoined { get => arePlayerJoined; }

    private void Start()
    {
        arePlayerJoined = false;
    }

    private void OnCollisionEnter(Collision collision)
    {
        int playerLayerNumber = (int) Mathf.Log(playerLayer.value, 2.0f);

        if (collision.gameObject.layer == playerLayerNumber)
            arePlayerJoined = true;
            
    }
}
