﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTwo : Player
{
    [SerializeField] private AudioSource invisibility;
    [SerializeField] private AudioSource backToNormal;

    private void Awake()
    {
        invisibility = Instantiate(invisibility, this.transform);
        backToNormal = Instantiate(backToNormal, this.transform);
    }

    public override void Move()
    {
        Vector3 moveLeft = Vector3.left * Speed * Time.deltaTime;

        if (this.rigidBody.velocity.x <= MINIMUM_TRESHOLD_FOR_MOVING)
            this.rigidBody.AddRelativeForce(moveLeft);
    }

    public void PlayHideSFX()
    {
        invisibility.PlayOneShot(invisibility.clip);
    }

    public void PlayBackToNormalSFX()
    {
        backToNormal.loop = true;
        backToNormal.Play();
        StartCoroutine(StopClip());
    }

    private IEnumerator StopClip()
    {
        yield return new WaitForSeconds(1.404f);
        backToNormal.loop = false;
        backToNormal.Stop();
    }
}
