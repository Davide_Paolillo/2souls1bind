﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hide : MonoBehaviour
{
    [Header("Conmponents")]
    [SerializeField] private Renderer body;

    [Header("Statistics")]
    [SerializeField] private float colorOverTimeFactor = 3.0f;
    [SerializeField] private float durationOfInvisibilityInSeconds = 1.0f;
    [SerializeField] private float abilityCooldownInSeconds = 2.0f;

    private int initalLayer;

    private Color originalColor;
    private Color hideColor;

    private bool notInCooldown;

    private const float MINIMUM_ALPHA_CHANNEL_VALUE = 0.004f;
    private const float MAXIMUM_ALPHA_CHANNEL_VALUE = 1.0f;
    private const int TRANSPARENT_FX_LAYER = 1;

    private void Start()
    {
        originalColor = GetPlayerColor();
        hideColor = new Color(originalColor.a, originalColor.g,
            originalColor.b, MINIMUM_ALPHA_CHANNEL_VALUE);
        notInCooldown = true;
        initalLayer = this.transform.GetChild(0).gameObject.layer;
    }

    private void Update()
    {
        Color currentPlayerColor = GetPlayerColor();

        if (Input.GetKeyDown(KeyCode.RightCommand) &&
            currentPlayerColor.a == MAXIMUM_ALPHA_CHANNEL_VALUE && notInCooldown)
        {
            Singleton<PlayerTwo>.Instance.PlayHideSFX();
            StartCoroutine(ChangeColorToTransparent());
        } 
    }

    #region COLOR_OPERATIONS

    private Color GetPlayerColor()
    {
        return body.material.GetColor("_BaseColor");
    }

    private IEnumerator ChangeColorToTransparent()
    {
        while (notInCooldown)
        {
            BecomeUnhittable();
            Color newColor = GetNewColorWithLessTransparency();

            if (newColor.a >= MINIMUM_ALPHA_CHANNEL_VALUE)
                body.material.SetColor("_BaseColor", newColor);
            else
            {
                body.material.SetColor("_BaseColor", hideColor);
                yield return new WaitForSeconds(durationOfInvisibilityInSeconds);
                StartCoroutine(ChangeColorToOriginal());
                yield break;
            }

            yield return new WaitForSeconds(0.001f);
        }
    }

    private Color GetNewColorWithLessTransparency()
    {
        Color currentColor = body.material.GetColor("_BaseColor");
        Color newColor = new Color(currentColor.r, currentColor.g,
            currentColor.b, currentColor.a -
            (colorOverTimeFactor * Time.deltaTime));
        return newColor;
    }

    private IEnumerator ChangeColorToOriginal()
    {
        Singleton<PlayerTwo>.Instance.PlayBackToNormalSFX();
        yield return new WaitForSeconds(0.5f);

        while (true)
        {
            BecomeHittable();
            Color newColor = GetNewColorWithMoreTransparency();
            notInCooldown = false;

            if (newColor.a <= MAXIMUM_ALPHA_CHANNEL_VALUE)
                body.material.SetColor("_BaseColor", newColor);
            else
            {
                body.material.SetColor("_BaseColor", originalColor);
                StartCoroutine(StartCooldown());
                yield break;
            }

            yield return new WaitForSeconds(0.001f);
        }
    }

    private Color GetNewColorWithMoreTransparency()
    {
        Color currentColor = body.material.GetColor("_BaseColor");
        Color newColor = new Color(currentColor.r, currentColor.g,
            currentColor.b, currentColor.a +
            (colorOverTimeFactor * Time.deltaTime));
        return newColor;
    }

    #endregion

    #region MECHANICS

    private IEnumerator StartCooldown()
    {
        yield return new WaitForSeconds(abilityCooldownInSeconds);
        notInCooldown = true;
    }

    private void BecomeUnhittable()
    {
        this.transform.GetChild(0).gameObject.layer = TRANSPARENT_FX_LAYER;
    }

    private void BecomeHittable()
    {
        this.transform.GetChild(0).gameObject.layer = initalLayer;
    }

    #endregion
}
