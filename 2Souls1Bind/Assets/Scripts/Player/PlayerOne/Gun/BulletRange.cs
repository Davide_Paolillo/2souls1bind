﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletRange : MonoBehaviour
{
    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<CapsuleProjectile>())
            Destroy(other.gameObject);
    }
}
