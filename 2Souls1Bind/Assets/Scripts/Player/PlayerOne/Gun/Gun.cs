﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
    [SerializeField] private Projectile projectile;

    private void Update()
    {
        ShootOnSpacePressed();
    }

    private void ShootOnSpacePressed()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Singleton<PlayerOne>.Instance.PlayShotSFX();
            Instantiate(this.projectile, this.transform.position,
                this.projectile.transform.rotation, this.transform); ;
        } 
    }
}
