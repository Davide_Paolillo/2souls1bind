﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerOne : Player
{
    [SerializeField] private AudioSource shot;

    private void Awake()
    {
        shot = Instantiate(shot, this.transform);
    }

    public override void Move()
    {
        Vector3 moveRight = Vector3.right * Speed * Time.deltaTime;

        if (this.rigidBody.velocity.x <= MINIMUM_TRESHOLD_FOR_MOVING)
            this.rigidBody.AddRelativeForce(moveRight);
    }

    public void PlayShotSFX()
    {
        shot.PlayOneShot(shot.clip);
    }
}
