﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Player : MonoBehaviour
{
    [Header("General Statistics")]
    [Range(0, 100)] [SerializeField] private int health = 1;

    [Header("Movement")]
    [Range(1, 10)] [SerializeField] private int speed = 1;

    [Header("Jump")]
    [Range(1, 500)] [SerializeField] private int jumpForce = 20;
    [SerializeField] private float maxJumpHeight = 2.0f;
    [SerializeField] private float fallMultiplier = 2.5f;
    [SerializeField] private float lowJumpMultiplier = 2.0f;

    [Header("Collisions Handling")]
    [SerializeField] private LayerMask wallsLayer;
    [SerializeField] private Feet feet;

    [Header("VFX")]
    [SerializeField] private GameObject deathParticles;

    [Header("SFX")]
    [SerializeField] private AudioSource jump;
    [SerializeField] private AudioSource fall;
    [SerializeField] private AudioSource[] hitted;

    public int Speed { get => speed; }
    public int JumpForce { get => jumpForce; }
    public int Health {
        get
        {
            int percentageHealth = health * 100 / startHealth;
            return percentageHealth >= 0 ? percentageHealth : 0;
        }
    }

    private int startHealth;

    protected Rigidbody rigidBody;

    private float initialMaxJumpHeight;

    private Body body;

    protected const float MINIMUM_TRESHOLD_FOR_JUMPING = 0.1f;
    protected const float MINIMUM_TRESHOLD_FOR_MOVING = 12.0f;

    protected const int MOVING_FORCE_FACTOR_MULTIPLIER = 400;

    private void Start()
    {
        speed *= MOVING_FORCE_FACTOR_MULTIPLIER;
        startHealth = health;
        initialMaxJumpHeight = maxJumpHeight;
        GetPlayerComponents();
        InstantiateAudio();
    }

    #region INSTANTIATIONS

    private void GetPlayerComponents()
    {
        this.rigidBody = this.GetComponentInChildren<Rigidbody>();
        body = GetComponentInChildren<Body>();
    }

    private void InstantiateAudio()
    {
        jump = Instantiate(jump, this.transform);
        fall = Instantiate(fall, this.transform);

        for (int i = 0; i < hitted.Length; i++)
        {
            hitted[i] = Instantiate(hitted[i], this.transform);
        }
    }

    #endregion

    private void Update()
    {
        IsAlive();
        IsEndGame();
    }

    #region ACTIONS

    public virtual void Jump()
    {
        if (initialMaxJumpHeight > 0)
        {
            this.rigidBody.velocity += (Vector3.up * jumpForce * Time.deltaTime);
            this.initialMaxJumpHeight -= Time.deltaTime;
        }
        else if (!IsGrounded())
        {
            this.rigidBody.velocity += Vector3.up * Physics.gravity.y * fallMultiplier * Time.deltaTime;
        }
        else
        {
            ResetYVelocity();
        }
    }

    public void Fall()
    {
        if (!IsGrounded())
        {
            this.rigidBody.velocity += Vector3.up * Physics.gravity.y * lowJumpMultiplier * Time.deltaTime;
            this.initialMaxJumpHeight = 0;
        }

        if (IsGrounded())
        {
            ResetYVelocity();
            this.initialMaxJumpHeight = this.maxJumpHeight;
        }
    }

    public abstract void Move();

    public void TakeDamage(int damageToDeal)
    {
        PlayHitSFX();
        health -= damageToDeal;
    }

    public void TakeFatalDamage()
    {
        PlayHitSFX();
        health = 0;
    }

    private void KillPlayer()
    {
        DisableComponents();
        ResetXVelocity();
        PlayDeathAnimation();
        this.enabled = false;
    }

    #endregion

    #region CHECKS

    private bool IsGrounded()
    {
        return feet.IsGround;
    }

    private void IsAlive()
    {
        if (health <= 0)
            KillPlayer();
    }

    public bool IsEndGame()
    {
        if (body.ArePlayerJoined)
            return EndLevel();

        return false;
    }

    private bool EndLevel()
    {
        // TODO: Implement VFX for winning th level
        Time.timeScale = 0.0f;
        return true;
    }

    #endregion

    #region RESETTERS

    private void ResetYVelocity()
    {
        this.rigidBody.velocity = new Vector3(this.rigidBody.velocity.x, 0.0f, this.rigidBody.velocity.z);
    }

    public void ResetXVelocity()
    {
        this.rigidBody.velocity = new Vector3(0.0f, this.rigidBody.velocity.y, this.rigidBody.velocity.z);
    }

    private void DisableComponents()
    {
        this.gameObject.GetComponentInChildren<MeshRenderer>().enabled = false;
        this.transform.GetChild(0).GetComponentInChildren<Light>().enabled = false;
        if (this.transform.GetChild(0).GetComponentInChildren<Gun>())
            this.transform.GetChild(0).GetComponentInChildren<Gun>().enabled = false;
        this.rigidBody.isKinematic = true;
    }

    #endregion

    #region SFX

    private void PlayHitSFX()
    {
        int index = Random.Range(0, hitted.Length);
        hitted[index].PlayOneShot(hitted[index].clip);
    }

    public void PlayJumpSFX()
    {
        jump.PlayOneShot(jump.clip);
    }

    public void PlayFallSFX()
    {
        StartCoroutine(PlaySoundWhenPlayerTouckGround());
    }

    private IEnumerator PlaySoundWhenPlayerTouckGround()
    {
        while (!IsGrounded())
        {
            yield return new WaitForSeconds(0.01f);
        }

        fall.PlayOneShot(fall.clip);
        yield break;
    }

    #endregion

    #region VFX

    private void PlayDeathAnimation()
    {
        Instantiate(deathParticles, this.transform.GetChild(0).position, this.transform.rotation);
    }

    #endregion
}
