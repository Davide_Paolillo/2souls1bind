﻿using System.Collections;
using System.Collections.Generic;
using Michsky.UI.ModernUIPack;
using UnityEngine;

public class HealthBar : MonoBehaviour
{
    [SerializeField] private ProgressBar healthBar;

    private Player currentPlayer;

    private void Start()
    {
        currentPlayer = GetComponent<Player>();
    }

    private void Update()
    {
        healthBar.currentPercent = currentPlayer.Health;
    }
}
