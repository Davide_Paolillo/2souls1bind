﻿public static class InputFactory
{
    public static InputType GetInputType(bool coordinatedMovement, bool coordinatedJump, bool coordinatedJumpMovement)
    {
        if (coordinatedMovement)
        {
            return new CoordinatedMovement();
        }
        else if (coordinatedJump)
        {
            return new CoordinatedJump();
        }
        else if (coordinatedJumpMovement)
        {
            return new CoordinatedJumpMovement();
        }

        return new NormalMovement();
    }
}
