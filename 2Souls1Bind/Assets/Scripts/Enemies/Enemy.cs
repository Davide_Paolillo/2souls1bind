﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Enemy : MonoBehaviour
{
    [Header("Statistics")]
    [SerializeField] private int health = 1;
    //[SerializeField] private int speed = 1;

    [Header("Attack")]
    [SerializeField] private GameObject projectile;

    [Header("Utilities")]
    [SerializeField] private LayerMask hitLayer;

    [Header("VFX")]
    [SerializeField] private GameObject deathVFX;

    [Header("SFX")]
    [SerializeField] private AudioSource[] death;
    [SerializeField] private AudioSource shot;

    protected GameObject target;

    private int enemyLayerNumber;

    public GameObject Projectile { get => projectile; }

    private void Awake()
    {
        enemyLayerNumber = (int) Mathf.Log(hitLayer, 2.0f);
        InstantiateAudio();
    }

    private void InstantiateAudio()
    {
        shot = Instantiate(shot, this.transform);

        for (int i = 0; i < death.Length; i++)
        {
            death[i] = Instantiate(death[i], this.transform);
        }
    }

    private void Update()
    {
        if (health <= 0)
            KillEnemy();
    }

    private void OnTriggerStay(Collider other)
    {
        target = other.gameObject;

        if (other.gameObject.layer == enemyLayerNumber)
            Shot();
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer == enemyLayerNumber)
            target = null;
    }

    protected abstract void Shot();

    public virtual void TakeDamage(int damage)
    {
        health -= damage;
        PlayEnemyHittedSFX();
    }

    protected virtual void KillEnemy()
    {
        Instantiate(deathVFX, this.transform.position, this.transform.rotation);
        PlayEnemyHittedSFX();
        Destroy(this.gameObject);
    }

    private void PlayEnemyHittedSFX()
    {
        int randomIndex = Random.Range(0, death.Length);
        death[randomIndex].PlayOneShot(death[randomIndex].clip);
    }

    protected void PlayEnemyShotSFX()
    {
        shot.PlayOneShot(shot.clip);
    }
}
