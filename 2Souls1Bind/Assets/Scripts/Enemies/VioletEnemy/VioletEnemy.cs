﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VioletEnemy : Enemy
{
    [SerializeField] private float projectileCooldownInSeconds = 0.4f;

    private bool onCooldown;

    private void Start()
    {
        onCooldown = false;
    }

    protected override void Shot()
    {
        if (!onCooldown && target && target.GetComponentInParent<Player>().Health > 0)
            ShotProjectile();
    }

    private void ShotProjectile()
    {
        GameObject newProjectile = Instantiate(this.Projectile,
                    this.transform.GetChild(0).position,
                    this.transform.rotation, this.transform);
        PlayEnemyShotSFX();

        newProjectile.GetComponent<SphereProjectile>().Target = target;

        onCooldown = true;
        StartCoroutine(StartCooldown());
    }

    private IEnumerator StartCooldown()
    {
        yield return new WaitForSeconds(projectileCooldownInSeconds);
        onCooldown = false;
    }
}
