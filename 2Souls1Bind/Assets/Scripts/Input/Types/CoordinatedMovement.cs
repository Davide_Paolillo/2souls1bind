﻿using UnityEngine;

public class CoordinatedMovement : InputType
{
    public void MovePlayer()
    {
        if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.LeftArrow))
        {
            Singleton<PlayerOne>.Instance.Move();
            Singleton<PlayerTwo>.Instance.Move();
        }

        if (Input.GetKeyUp(KeyCode.D) || Input.GetKeyUp(KeyCode.LeftArrow))
        {
            Singleton<PlayerOne>.Instance.ResetXVelocity();
            Singleton<PlayerTwo>.Instance.ResetXVelocity();
        }
    }

    public void LetPlayerJump()
    {
        if (Input.GetKeyDown(KeyCode.W))
        {
            Singleton<PlayerOne>.Instance.PlayJumpSFX();
            Singleton<PlayerOne>.Instance.Jump();
        }
        else if (Input.GetKey(KeyCode.W))
            Singleton<PlayerOne>.Instance.Jump();
        else if (Input.GetKeyUp(KeyCode.W))
        {
            Singleton<PlayerOne>.Instance.PlayFallSFX();
            Singleton<PlayerOne>.Instance.Fall();
        }
        else
            Singleton<PlayerOne>.Instance.Fall();

        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            Singleton<PlayerTwo>.Instance.PlayJumpSFX();
            Singleton<PlayerTwo>.Instance.Jump();
        }
        else if (Input.GetKey(KeyCode.UpArrow))
            Singleton<PlayerTwo>.Instance.Jump();
        else if (Input.GetKeyUp(KeyCode.UpArrow))
        {
            Singleton<PlayerTwo>.Instance.PlayFallSFX();
            Singleton<PlayerTwo>.Instance.Fall();
        }
        else
            Singleton<PlayerTwo>.Instance.Fall();
    }
}
