﻿using UnityEngine;

public class CoordinatedJumpMovement : InputType
{
    public void MovePlayer()
    {
        if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.LeftArrow))
        {
            Singleton<PlayerOne>.Instance.Move();
            Singleton<PlayerTwo>.Instance.Move();
        }

        if(Input.GetKeyUp(KeyCode.D) || Input.GetKeyUp(KeyCode.LeftArrow))
        {
            Singleton<PlayerOne>.Instance.ResetXVelocity();
            Singleton<PlayerTwo>.Instance.ResetXVelocity();
        }
    }

    public void LetPlayerJump()
    {
        if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow))
        {
            Singleton<PlayerOne>.Instance.PlayJumpSFX();
            Singleton<PlayerOne>.Instance.Jump();
            Singleton<PlayerTwo>.Instance.PlayJumpSFX();
            Singleton<PlayerTwo>.Instance.Jump();
        }
        else if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
        {
            Singleton<PlayerOne>.Instance.Jump();
            Singleton<PlayerTwo>.Instance.Jump();
        }
        else if (Input.GetKeyUp(KeyCode.W) || Input.GetKeyUp(KeyCode.UpArrow))
        {
            Singleton<PlayerOne>.Instance.PlayFallSFX();
            Singleton<PlayerOne>.Instance.Fall();
            Singleton<PlayerTwo>.Instance.PlayFallSFX();
            Singleton<PlayerTwo>.Instance.Fall();
        }
        else
        {
            Singleton<PlayerOne>.Instance.Fall();
            Singleton<PlayerTwo>.Instance.Fall();
        }
    }
}
