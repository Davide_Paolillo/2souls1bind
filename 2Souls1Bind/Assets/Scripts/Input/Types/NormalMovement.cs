﻿using UnityEngine;

public class NormalMovement : InputType
{
    public void MovePlayer()
    {
        if (Input.GetKey(KeyCode.D))
            Singleton<PlayerOne>.Instance.Move();
        else if (Input.GetKeyUp(KeyCode.D))
            Singleton<PlayerOne>.Instance.ResetXVelocity();

        if (Input.GetKey(KeyCode.LeftArrow))
            Singleton<PlayerTwo>.Instance.Move();
        else if (Input.GetKeyUp(KeyCode.LeftArrow))
            Singleton<PlayerTwo>.Instance.ResetXVelocity();
    }

    public void LetPlayerJump()
    {
        if (Input.GetKeyDown(KeyCode.W))
        {
            Singleton<PlayerOne>.Instance.PlayJumpSFX();
            Singleton<PlayerOne>.Instance.Jump();
        }  
        else if (Input.GetKey(KeyCode.W))
            Singleton<PlayerOne>.Instance.Jump();
        else if (Input.GetKeyUp(KeyCode.W)) {
            Singleton<PlayerOne>.Instance.PlayFallSFX();
            Singleton<PlayerOne>.Instance.Fall();
        }
        else
            Singleton<PlayerOne>.Instance.Fall();

        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            Singleton<PlayerTwo>.Instance.PlayJumpSFX();
            Singleton<PlayerTwo>.Instance.Jump();
        }
        else if (Input.GetKey(KeyCode.UpArrow))
            Singleton<PlayerTwo>.Instance.Jump();
        else if (Input.GetKeyUp(KeyCode.UpArrow))
        {
            Singleton<PlayerTwo>.Instance.PlayFallSFX();
            Singleton<PlayerTwo>.Instance.Fall();
        }
        else
            Singleton<PlayerTwo>.Instance.Fall();
    }
}
