﻿using UnityEngine;

public class CoordinatedJump : InputType
{
    public void MovePlayer()
    {
        if (Input.GetKey(KeyCode.D))
            Singleton<PlayerOne>.Instance.Move();
        else if (Input.GetKeyUp(KeyCode.D))
            Singleton<PlayerOne>.Instance.ResetXVelocity();

        if (Input.GetKey(KeyCode.LeftArrow))
            Singleton<PlayerTwo>.Instance.Move();
        else if (Input.GetKeyUp(KeyCode.LeftArrow))
            Singleton<PlayerTwo>.Instance.ResetXVelocity();
    }

    public void LetPlayerJump()
    {
        if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow))
        {
            Singleton<PlayerOne>.Instance.PlayJumpSFX();
            Singleton<PlayerOne>.Instance.Jump();
            Singleton<PlayerTwo>.Instance.PlayJumpSFX();
            Singleton<PlayerTwo>.Instance.Jump();
        }
        else if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
        {
            Singleton<PlayerOne>.Instance.Jump();
            Singleton<PlayerTwo>.Instance.Jump();
        }   
        else if (Input.GetKeyUp(KeyCode.W) || Input.GetKeyUp(KeyCode.UpArrow))
        {
            Singleton<PlayerOne>.Instance.PlayFallSFX();
            Singleton<PlayerOne>.Instance.Fall();
            Singleton<PlayerTwo>.Instance.PlayFallSFX();
            Singleton<PlayerTwo>.Instance.Fall();
        }
        else
        {
            Singleton<PlayerOne>.Instance.Fall();
            Singleton<PlayerTwo>.Instance.Fall();
        }
    }
}
